ARG PHP_VERSION

# build and run with docker-compose up --build
# rerun without forcing rebuild docker-compose up
FROM php:${PHP_VERSION}-fpm-alpine
ARG PORT
ARG COMPOSER_VERSION
ARG WORKDIR=/var/www/html
ARG XDEBUG_VERSION=3.3.1
ARG XDEBUG_HOST=host.docker.internal
ARG XDEBUG_PORT=9003

WORKDIR ${WORKDIR}

# install libraries
RUN apk update && apk upgrade && \
    apk add --no-cache $PHPIZE_DEPS autoconf g++ make linux-headers

# Install system dependencies and PHP extensions
RUN apk --update add --no-cache \
    icu-dev \
    oniguruma-dev \
    libxml2-dev \
    libpng-dev \
    libzip-dev \
    freetype-dev \
    libjpeg-turbo-dev \
    git \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install intl calendar zip soap pdo_mysql gd mysqli

# install xdebug
RUN pecl install xdebug-${XDEBUG_VERSION} && docker-php-ext-enable xdebug

# install bcmath
RUN docker-php-ext-install bcmath

# install pcntl
RUN docker-php-ext-install pcntl

# install sockets
RUN docker-php-ext-install sockets

#dev tools
RUN apk add iputils

# composer installation
COPY --from=composer /usr/bin/composer /usr/bin/composer

# PHP configuration
COPY php/php.ini /usr/local/etc/php/conf.d/php-additional.ini

#set xdebug configuration
RUN echo "xdebug.client_host=${XDEBUG_HOST}" >> /usr/local/etc/php/conf.d/php-additional.ini
RUN echo "xdebug.client_port=${XDEBUG_PORT}" >> /usr/local/etc/php/conf.d/php-additional.ini

#set port for php-fpm
RUN sed -i "s|9000|${PORT}|" /usr/local/etc/php-fpm.d/zz-docker.conf

RUN chown -R www-data:www-data ${WORKDIR}

USER www-data

# start services
CMD ["php-fpm"]

EXPOSE ${PORT}
