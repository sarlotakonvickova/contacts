<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240119084507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add slug to users';
    }

    public function up(Schema $schema): void
    {
	    // this up() migration is auto-generated, please modify it to your needs
		$this->addSql('ALTER TABLE `users` ADD `slug` VARCHAR(255) NOT NULL AFTER `note`');
		//$this->addSql('ALTER TABLE `users` ADD UNIQUE `unique_slug` (`slug`)');

    }

    public function down(Schema $schema): void
    {
	    // this down() migration is auto-generated, please modify it to your needs
       	$this->addSql('ALTER TABLE `users` DROP `slug`');

    }
}
