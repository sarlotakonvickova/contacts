<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('name', TextType::class, [
				'attr' => ['placeholder' => 'Jméno *'],
				'label' => false
			])
			->add('surname', TextType::class, [
				'attr' => ['placeholder' => 'Příjmení *'],
				'label' => false
			])
			->add('phone', TelType::class, [
				'attr' => ['placeholder' => 'Telefon'],
				'label' => false,
				'required' => false
			])
			->add('email', EmailType::class, [
				'attr' => ['placeholder' => 'E-mail *'],
				'label' => false
			])
			->add('note', TextareaType::class, [
				'attr' => ['placeholder' => 'Poznámka'],
				'label' => false,
				'required' => false
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => User::class
		]);
	}
}
