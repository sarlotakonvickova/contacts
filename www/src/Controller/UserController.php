<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserForm;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class UserController extends AbstractController
{

	private EntityManagerInterface $entityManager;

	private UserRepository $userRepository;

	private PaginatorInterface $paginator;

	public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, PaginatorInterface $paginator)
	{
		$this->entityManager = $entityManager;
		$this->userRepository = $userRepository;
		$this->paginator = $paginator;
	}

    #[Route('/', name: 'user_index', methods: ['GET'])]
    public function index(Request $request): Response
    {
		$users = $this->paginator->paginate(
			$this->userRepository->findAll(),
			$request->query->getInt('page', 1),
			10
		);
        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route('/new', name: 'user_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserForm::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $user->updateSlug();
			$slug = $user->getSlug();

	        if ($slug === null || !$this->userRepository->isSlugUnique($slug)) {
		        $this->addFlash('error', 'A user with this slug already exists or the slug is invalid.');
		        return $this->render('user/new.html.twig', [
			        'user' => $user,
			        'form' => $form,
		        ]);
	        }

	        $this->entityManager->persist($user);
	        $this->entityManager->flush();

	        return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

	#[Route('/{slug}', name: 'user_show', methods: ['GET'])]
	public function show(string $slug): Response
	{
		$user = $this->userRepository->findOneBy(['slug' => $slug]);

		return $this->render('user/show.html.twig', [
			'user' => $user,
		]);
	}

    #[Route('/{id}/edit', name: 'user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserForm::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $this->entityManager->flush();

            return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user): Response
    {
	    $token = (string) $request->request->get('_token');

	    if ($this->isCsrfTokenValid('delete'.$user->getId(), $token)) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
    }
}
