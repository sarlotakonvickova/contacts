# Base information
All projects are in the www folder.
map your local www folder to the docker container www folder.
```
/var/www/html
```
Use `docker-compose-import.yml` for enable projects.

php's ports has convention 90{php version} eg. 9081 for php 8.1

## Before start
- copy ``example.env`` to ``.env``
    - change values of ``MYSQL_ROOT_PASSWORD`` and ``MYSQL_DATABASE``
- change ``server_name`` to your domain in ``nginx/conf/contacts.conf``
- copy ``/www/example.env`` to ``www/.env``
  - change ``DATABASE_URL`` and change ``user``, ``password`` and ``database`` to your values

## Start command
run ``docker-compose up -d``

### First start
run ``docker-compose up``

### Database
- to create database run ``docker-compose exec php bin/console doctrine:database:create``
- to add run ``php bin/console doctrine:migrations:migrate``
- for GUI go to ``adminer.localhost`` or you can change it in ``nginx/conf/adminer.conf``

### PHPStan
- change name of ``www/phpstan.example.neon`` to ``www/phpstan.neon``
- run ``docker-compose exec php vendor/bin/phpstan analyse``
