FROM nginx

RUN apt-get update -y
RUN apt-get install inotify-tools -y

COPY ./www /var/www/html

COPY ./nginx/conf /etc/nginx/conf.d
